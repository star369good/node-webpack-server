import React, { useEffect } from "react";

export default function Bypass() {
    useEffect(() => {
        fetch('/api/bypass-example')
        .then(res => res.json())
        .then(data => console.log(data))
    }, []);
    return (
        <h1>Hello world!</h1>
    );
}