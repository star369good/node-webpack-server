import React from 'react';
import ReactDOM from 'react-dom';
import Bypass from './components/Bypass';

const App = () => {

  return (
      <>
        <Bypass />
      </>
  );
}

ReactDOM.render(<App />, document.getElementById('root'));